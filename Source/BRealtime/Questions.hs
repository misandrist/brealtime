{-# Language TemplateHaskell, DataKinds, PolyKinds, TypeFamilies,
  ScopedTypeVariables, FlexibleInstances, UndecidableInstances, GADTs #-}

{-|
Module:             Requests
Description:        Singletons for parsing the requests.
Copyright:          © 2017 All rights reserved.
License:            AGPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Questions (
    -- * Questions
    Question,
    -- * Answers
    Answer,
    answer
    ) where

import Lawless
import Textual
import Printer hiding(text)
import Parser
import Data.Ord
import Text.Read
import Servant.API
import Puzzle

-- | The 'Question's that the remote endpoint asks.
--
-- I'm not quite sure this is comprehensive yet, but I'm working
-- through what's coming through to see.
data Question =
    Ping |
    Years |
    Position |
    Status |
    Source |
    Degree |
    Puzzle |
    Name |
    EmailAddress |
    Referrer |
    Phone |
    Resume
    deriving (Eq, Ord, Show, Read)

-- | A 'Question' is convertible to several representations using 'Printable'.
--
-- 'Printable' provides a set of combinators for printing
-- human-readable structures. When combined with 'Textual' for
-- parsing, it forms a pair of morphisms between a type and various
-- string and 'Text' representations.
instance Printable Question where
    print = \case
        EmailAddress → "Email Address"
        q → print $ show q

-- | A 'Question' is recoverable from several character streams.
--
-- This is obviously not bijective, since there are plenty of streams
-- which wouldn't represent a 'Question'. However, this is represented
-- by the underlying monad, 'Parsed', which has a 'Parsed' state and a
-- 'Malformed' state. There are then functions which will convert
-- these to 'Maybe' or 'Either', in addition to the underlying
-- 'Parsed' representation.
--
-- We make use of the derived 'Read' instance for parsing the
-- 'Question', except for 'EmailAddress', which has a space in the @q@
-- sent from the remote endpoint.
instance Textual Question where
    textual =
        let
            r = (some letter ≫= \l → case readEither l of
                        Left e → unexpected e
                        Right q → return q)
        in
            (EmailAddress <$ text "Email Address") <|> r

-- | 'Question' can be a query parameter.
--
-- We use the 'Textual' instance of 'Question' to parse the @q@ query
-- parameter.
instance FromHttpApiData Question where
    parseQueryParam p = case fromText p of
        Just q → Right q
        Nothing → Left p

-- | An 'Answer' is a wrapper around the 'Question', if it exists.
--
-- This actually stores the 'Maybe' 'Question' that the 'QueryParam'
-- captures parameters as. If there's no @q@, there's no 'Question',
-- and 'unAnswer' will be 'Nothing'.
--
-- Otherwise, it will be 'Just' 'Question', and the 'Question' will be
-- the value representing the 'Question' asked.
newtype Answer = Answer {unAnswer ∷ Maybe Question}

-- | 'Answer' and 'Maybe' 'Question' are isomorphic.
--
-- Since there are two fully bijective morphisms between 'Maybe
-- Question' and 'Answer', they form an isomorphism, and we can
-- express this by defining an 'Iso'' between them.
--
-- This then lets us run the standard "Control.Lens" operators '^.' to
-- get an 'Answer' from a 'Maybe' 'Question', and it's inverse, '^.re'
-- to get a 'Maybe' 'Question' from an 'Answer'.
answer ∷ Iso' (Maybe Question) Answer
answer = iso Answer unAnswer

-- | Convert an 'Answer' to a stream of characters.
--
-- This is indexed to the underlying 'Question', or, if there was
-- none, it returns 'NG.
instance Printable Answer where
    print a = case unAnswer a of
        Just q → case q of
            Ping → "OK"
            Years → "25"
            Position → "Software Engineer"
            Status → "Yes"
            Source → "https://gitlab.com/misandrist/brealtime"
            Degree → "N/A"
            -- Puzzle → print puzzle
            Name → "Evan Cofsky"
            EmailAddress → "evan@theunixman.com"
            Referrer → "Amanda Geller"
            Phone → "213-235-7495, +44 7428 086553"
            Resume → "https://theunixman.com/assets/files/EvanCofsky2017.pdf"
        Nothing → "NG"

-- | Render an 'Answer' to a 'PlainText' body.
--
-- This uses the function form of the "Control.Lens" operators '^.'
-- ('view'), and the 'lazy' 'Lens'es.
--
-- 'toUtf8' will convert a 'Printable' to a strict
-- 'Data.ByteString.ByteString', but the 'MimeRender' expects a
-- 'Data.ByteString.Lazy.ByteString', so we use the 'lazy' 'Lens' to
-- convert it.
instance MimeRender PlainText Answer where
    mimeRender _ = view lazy ∘ toUtf8
