(
 (nil . (
         (projectile-project-compilation-dir . "/")
         (projectile-project-compilation-cmd . "cabal new-build")
         (haskell-process-type . 'cabal-new-repl)
         )
      )
 )
